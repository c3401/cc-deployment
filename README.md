# Deployment of CC Services

Follwoing figure discussed the architecture of the CC platfrom. 

![Alt text](system_architecture.png?raw=true "CC platfrom architecture")

## Services

There are three main servies;

```
1. elassandra
2. aplos
3. gateway
```

## Deploy services

Start services in following order;

```
docker-compose up -d elassandra
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d web
```

** After hosting website, it can be reached on `<Ip address of the Vm>:4300`.
** Open `7654` and `4300` port on VM for public

